#if _DEBUG
#pragma comment(lib, "opencv_aruco331d.lib")
#pragma comment(lib, "opencv_bgsegm331d.lib")
#pragma comment(lib, "opencv_bioinspired331d.lib")
#pragma comment(lib, "opencv_calib3d331d.lib")
#pragma comment(lib, "opencv_ccalib331d.lib")
#pragma comment(lib, "opencv_core331d.lib")
#pragma comment(lib, "opencv_cudaarithm331d.lib")
#pragma comment(lib, "opencv_cudabgsegm331d.lib")
#pragma comment(lib, "opencv_cudacodec331d.lib")
#pragma comment(lib, "opencv_cudafeatures2d331d.lib")
#pragma comment(lib, "opencv_cudafilters331d.lib")
#pragma comment(lib, "opencv_cudaimgproc331d.lib")
#pragma comment(lib, "opencv_cudalegacy331d.lib")
#pragma comment(lib, "opencv_cudaobjdetect331d.lib")
#pragma comment(lib, "opencv_cudaoptflow331d.lib")
#pragma comment(lib, "opencv_cudastereo331d.lib")
#pragma comment(lib, "opencv_cudawarping331d.lib")
#pragma comment(lib, "opencv_cudev331d.lib")
#pragma comment(lib, "opencv_datasets331d.lib")
#pragma comment(lib, "opencv_dnn331d.lib")
#pragma comment(lib, "opencv_dpm331d.lib")
#pragma comment(lib, "opencv_face331d.lib")
#pragma comment(lib, "opencv_features2d331d.lib")
#pragma comment(lib, "opencv_flann331d.lib")
#pragma comment(lib, "opencv_fuzzy331d.lib")
#pragma comment(lib, "opencv_highgui331d.lib")
#pragma comment(lib, "opencv_imgcodecs331d.lib")
#pragma comment(lib, "opencv_imgproc331d.lib")
#pragma comment(lib, "opencv_img_hash331d.lib")
#pragma comment(lib, "opencv_line_descriptor331d.lib")
#pragma comment(lib, "opencv_ml331d.lib")
#pragma comment(lib, "opencv_objdetect331d.lib")
#pragma comment(lib, "opencv_optflow331d.lib")
#pragma comment(lib, "opencv_phase_unwrapping331d.lib")
#pragma comment(lib, "opencv_photo331d.lib")
#pragma comment(lib, "opencv_plot331d.lib")
#pragma comment(lib, "opencv_reg331d.lib")
#pragma comment(lib, "opencv_rgbd331d.lib")
#pragma comment(lib, "opencv_saliency331d.lib")
#pragma comment(lib, "opencv_sfm331d.lib")
#pragma comment(lib, "opencv_shape331d.lib")
#pragma comment(lib, "opencv_stereo331d.lib")
#pragma comment(lib, "opencv_stitching331d.lib")
#pragma comment(lib, "opencv_structured_light331d.lib")
#pragma comment(lib, "opencv_superres331d.lib")
#pragma comment(lib, "opencv_surface_matching331d.lib")
#pragma comment(lib, "opencv_text331d.lib")
#pragma comment(lib, "opencv_tracking331d.lib")
#pragma comment(lib, "opencv_video331d.lib")
#pragma comment(lib, "opencv_videoio331d.lib")
#pragma comment(lib, "opencv_videostab331d.lib")
#pragma comment(lib, "opencv_viz331d.lib")
#pragma comment(lib, "opencv_xfeatures2d331d.lib")
#pragma comment(lib, "opencv_ximgproc331d.lib")
#pragma comment(lib, "opencv_xobjdetect331d.lib")
#pragma comment(lib, "opencv_xphoto331d.lib")
#else
#pragma comment(lib, "opencv_aruco331.lib")
#pragma comment(lib, "opencv_bgsegm331.lib")
#pragma comment(lib, "opencv_bioinspired331.lib")
#pragma comment(lib, "opencv_calib3d331.lib")
#pragma comment(lib, "opencv_ccalib331.lib")
#pragma comment(lib, "opencv_core331.lib")
#pragma comment(lib, "opencv_cudaarithm331.lib")
#pragma comment(lib, "opencv_cudabgsegm331.lib")
#pragma comment(lib, "opencv_cudacodec331.lib")
#pragma comment(lib, "opencv_cudafeatures2d331.lib")
#pragma comment(lib, "opencv_cudafilters331.lib")
#pragma comment(lib, "opencv_cudaimgproc331.lib")
#pragma comment(lib, "opencv_cudalegacy331.lib")
#pragma comment(lib, "opencv_cudaobjdetect331.lib")
#pragma comment(lib, "opencv_cudaoptflow331.lib")
#pragma comment(lib, "opencv_cudastereo331.lib")
#pragma comment(lib, "opencv_cudawarping331.lib")
#pragma comment(lib, "opencv_cudev331.lib")
#pragma comment(lib, "opencv_datasets331.lib")
#pragma comment(lib, "opencv_dnn331.lib")
#pragma comment(lib, "opencv_dpm331.lib")
#pragma comment(lib, "opencv_face331.lib")
#pragma comment(lib, "opencv_features2d331.lib")
#pragma comment(lib, "opencv_flann331.lib")
#pragma comment(lib, "opencv_fuzzy331.lib")
#pragma comment(lib, "opencv_highgui331.lib")
#pragma comment(lib, "opencv_imgcodecs331.lib")
#pragma comment(lib, "opencv_imgproc331.lib")
#pragma comment(lib, "opencv_img_hash331.lib")
#pragma comment(lib, "opencv_line_descriptor331.lib")
#pragma comment(lib, "opencv_ml331.lib")
#pragma comment(lib, "opencv_objdetect331.lib")
#pragma comment(lib, "opencv_optflow331.lib")
#pragma comment(lib, "opencv_phase_unwrapping331.lib")
#pragma comment(lib, "opencv_photo331.lib")
#pragma comment(lib, "opencv_plot331.lib")
#pragma comment(lib, "opencv_reg331.lib")
#pragma comment(lib, "opencv_rgbd331.lib")
#pragma comment(lib, "opencv_saliency331.lib")
#pragma comment(lib, "opencv_sfm331.lib")
#pragma comment(lib, "opencv_shape331.lib")
#pragma comment(lib, "opencv_stereo331.lib")
#pragma comment(lib, "opencv_stitching331.lib")
#pragma comment(lib, "opencv_structured_light331.lib")
#pragma comment(lib, "opencv_superres331.lib")
#pragma comment(lib, "opencv_surface_matching331.lib")
#pragma comment(lib, "opencv_text331.lib")
#pragma comment(lib, "opencv_tracking331.lib")
#pragma comment(lib, "opencv_video331.lib")
#pragma comment(lib, "opencv_videoio331.lib")
#pragma comment(lib, "opencv_videostab331.lib")
#pragma comment(lib, "opencv_viz331.lib")
#pragma comment(lib, "opencv_xfeatures2d331.lib")
#pragma comment(lib, "opencv_ximgproc331.lib")
#pragma comment(lib, "opencv_xobjdetect331.lib")
#pragma comment(lib, "opencv_xphoto331.lib")
#endif // _DEBUG

#pragma comment(lib, "correspondence.lib")
#pragma comment(lib, "multiview.lib")
#pragma comment(lib, "numeric.lib")
#pragma comment(lib, "simple_pipeline.lib")

//
//
//
//#define CERES_FOUND 1
////#define OPENCV_TRAITS_ENABLE_DEPRECATED
//
//#include <opencv2\opencv.hpp>
//#include <opencv2\sfm.hpp>
//#include <opencv2\viz.hpp>
//
//#include <iostream>
//#include <fstream>
//#include <string>
//
//using namespace std;
//using namespace cv;
//using namespace cv::sfm;
//
//int getdir(const string _filename, vector<string> &files) {
//	ifstream myfile(_filename.c_str());
//	if (!myfile.is_open()) {
//		cout << "Unable to read file: " << _filename << endl;
//		exit(0);
//	} else {
//		;
//		size_t found = _filename.find_last_of("/\\");
//		string line_str, path_to_file = _filename.substr(0, found);
//		while (getline(myfile, line_str))
//			files.push_back(path_to_file + string("/") + line_str);
//	}
//	return 1;
//}
//
//int main() {
//	// Parse the image paths
//	vector<string> images_paths;
//	getdir("temple/input.txt", images_paths);
//
//	// Build instrinsics
//	float f = 800,
//		cx = 400, cy = 255;
//	Matx33d K = Matx33d(f, 0, cx,
//						0, f, cy,
//						0, 0, 1);
//
//
//	bool is_projective = true;
//	vector<Mat> Rs_est, ts_est, points3d_estimated;
//	reconstruct(images_paths, Rs_est, ts_est, K, points3d_estimated, is_projective);
//
//	// Print output
//	cout << "\n----------------------------\n" << endl;
//	cout << "Reconstruction: " << endl;
//	cout << "============================" << endl;
//	cout << "Estimated 3D points: " << points3d_estimated.size() << endl;
//	cout << "Estimated cameras: " << Rs_est.size() << endl;
//	cout << "Refined intrinsics: " << endl << K << endl << endl;
//	cout << "3D Visualization: " << endl;
//	cout << "============================" << endl;
//	viz::Viz3d window("Coordinate Frame");
//	window.setWindowSize(Size(500, 500));
//	window.setWindowPosition(Point(150, 150));
//	window.setBackgroundColor(); // black by default
//								 // Create the pointcloud
//	cout << "Recovering points  ... ";
//	// recover estimated points3d
//	vector<Vec3f> point_cloud_est;
//	for (int i = 0; i < points3d_estimated.size(); ++i)
//		point_cloud_est.push_back(Vec3f(points3d_estimated[i]));
//	cout << "[DONE]" << endl;
//	cout << "Recovering cameras ... ";
//	vector<Affine3d> path;
//	for (size_t i = 0; i < Rs_est.size(); ++i)
//		path.push_back(Affine3d(Rs_est[i], ts_est[i]));
//	cout << "[DONE]" << endl;
//	if (point_cloud_est.size() > 0) {
//		cout << "Rendering points   ... ";
//		viz::WCloud cloud_widget(point_cloud_est, viz::Color::green());
//		window.showWidget("point_cloud", cloud_widget);
//		cout << "[DONE]" << endl;
//	} else {
//		cout << "Cannot render points: Empty pointcloud" << endl;
//	}
//	if (path.size() > 0) {
//		cout << "Rendering Cameras  ... ";
//		window.showWidget("cameras_frames_and_lines", viz::WTrajectory(path, viz::WTrajectory::BOTH, 0.1, viz::Color::green()));
//		window.showWidget("cameras_frustums", viz::WTrajectoryFrustums(path, K, 0.1, viz::Color::yellow()));
//		window.setViewerPose(path[0]);
//		cout << "[DONE]" << endl;
//	} else {
//		cout << "Cannot render the cameras: Empty path" << endl;
//	}
//	cout << endl << "Press 'q' to close each windows ... " << endl;
//	window.spin();
//	return 0;
//}


#include <opencv2\opencv.hpp>

const cv::String file("N:/workspace/samplevideo.mp4");

int main() {
	cv::VideoCapture m_Video;
	if (!m_Video.open("./sampleshakevideo.mp4")) {
		std::cout << "No vedeo" << std::endl;
		return 0;
	}


	return 0;
}
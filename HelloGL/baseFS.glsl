#version 430 core

in VOUT {
	vec4 color;
} fi;

out vec4 fcolor;

void main() {
	fcolor = vec4(1.f) - fi.color;
}
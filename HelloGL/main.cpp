#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <stdio.h>

#include "Shader.h"

int main() {
	glfwInit();							// 초기화
	glfwSetErrorCallback([](int e, const char *desc) {
		puts(desc);
	});
	GLFWwindow *m_Win = glfwCreateWindow(680, 420, "GLFW Window", 0, 0);	// 윈도우를 만든다.
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);			// GLFW profile  옵션 설정

	//int m, n;										// context를 만든 후 버전을 알 수가 있다.
	//glGetIntegerv(GL_MAJOR_VERSION, &m);
	//glGetIntegerv(GL_MINOR_VERSION, &n);
	//printf("GL Version: %d.%d\n", m, n);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);					// gl version 결정
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);					// gl version 결정
	glfwMakeContextCurrent(m_Win);						// 현재 context를 만들어준다.

	glewInit();
	// VAO
	GLuint m_VAO;				// vertex array object IDs
	glGenVertexArrays(1, &m_VAO);				// vao gen
	glBindVertexArray(m_VAO);					// vao id를 바인딩(조작하기 위해서는 꼭 함)

	GLuint m_VBO;								// vertext용 buffer
	float pos[] = { 
		// pos		// color
		0, 0,		1.f, 0.f, 0.f, 1.f,
		0, 1,		0.f, 1.f, 0.f, 1.f,
		1, 0,		0.f, 0.f, 1.f, 1.f
	};			// 점 정보들
	glGenBuffers(1, &m_VBO);					// vbo gen
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);		// vbo id를 바인딩
	glBufferData(GL_ARRAY_BUFFER, sizeof(pos), pos, GL_STATIC_DRAW);	// 데이터 삽입부분 크기 할당 및 데이터 삽입

	// position (0)
	glVertexAttribPointer(0, 2, GL_FLOAT, false, 6*sizeof(float), (void *)0);			// Att0에 position을 위한 buffer object를 연결
	glEnableVertexAttribArray(0);										// Att0을 활성화
	// color (1)
	glVertexAttribPointer(1, 4, GL_FLOAT, false, 6 * sizeof(float), (void *)(2 * sizeof(float)));			// Att0에 position을 위한 buffer object를 연결
	glEnableVertexAttribArray(1);										// Att0을 활성화

	glBindBuffer(GL_ARRAY_BUFFER, 0);									// GL_ARRAY_BUFFER 0으로 바인딩 (해제의 의미)
	//glBindVertexArray(0);												// VAO 0으로 바인딩

	// Shader
	Shader m_Shader("baseVS.glsl", "baseFS.glsl");

	while (!glfwWindowShouldClose(m_Win)) {
		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		m_Shader.use();
		glDrawArrays(GL_TRIANGLES, 0, 3);
		m_Shader.unuse();

		// End 
		glfwSwapBuffers(m_Win);
		glfwPollEvents();
	}

	glfwDestroyWindow(m_Win);			// 윈도우 하나 종료
	glfwTerminate();					// glfw 종료 (모든 윈도우 종료)

	return 0;
}
#version 430 core

layout(location = 0) in vec2 pos;
layout(location = 1) in vec4 color;

out VOUT {
	vec4 color;
} vo;

void main() {
	int i = gl_VertexID;
	gl_Position = vec4(pos - vec2(0.5), 0, 1);

	vo.color = color;
}